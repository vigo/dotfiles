#!/usr/bin/env ruby
# coding: utf-8

require "#{File.dirname(__FILE__)}/colors_prompt"

unless `which mysql.server`.strip.empty?
  mysql_status = `mysql.server status`.strip
  is_mysql_running = mysql_status.match(/running \((\d+)\)/)
  unless is_mysql_running.nil?
    puts "[#{$COLOR_GREEN_L}mysql#{$COLOR_RESET}] "
  end
end