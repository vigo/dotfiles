#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "#{File.dirname(__FILE__)}/colors_prompt"
screen = `screen -ls`.chomp
out = ""
prompt = ""
screen.scan(/(\d+)\.(\w+)/).map{ |id, name|
  out << "#{$COLOR_RED}#{id}#{$COLOR_RESET} (#{$COLOR_YELLOW_U}#{name}#{$COLOR_RESET}), "
  prompt << "#{name} #{id} "
}
out.chomp!(", ")

if ARGV.length > 0
  puts prompt
else
  puts "#{$COLOR_WHITE}Screen#{$COLOR_RESET}: #{out} " if out.length > 0 and ENV['TERM'] != 'screen'
end

#
  
  
