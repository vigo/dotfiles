#!/usr/bin/env ruby
# coding: utf-8

# modified from http://quickleft.com/blog/awesome-zsh-prompts-can-be-yours

require "#{File.dirname(__FILE__)}/colors_prompt"

$power_data = `pmset -g batt`
$percent_battery = $power_data.match(/\d+\%/).to_s.gsub("%","").to_f
$are_we_on_ac = $power_data.match(/AC Power/).to_s.empty? ? false : true

NOTIFIERS = {
  'growlnotify' => '-t "%s" -m "%s"',
  'terminal-notifier' => '-title "%s" -message "%s"',
}

def get_notifier
  NOTIFIERS.each do |notifier_exe, notifier_options|
    return [notifier_exe, notifier_options] unless `which #{notifier_exe}`.empty?
  end
  return false
end

def views style
  case style
  when "icon"
    empty = '▹'
    filled = '▸'
    color = $COLOR_GREEN
    if $percent_battery < 20
      color = $COLOR_RED
    elsif $percent_battery < 50
      color = $COLOR_YELLOW
    end
    num_filled = ($percent_battery/10).ceil
    ' ' + color + '[' + (filled * num_filled) + empty * (10-num_filled) + ']' + $COLOR_RESET
  when "hours"
    time_data = $power_data.match(/(\d*):(\d*) remaining/)
    unless time_data.to_s.empty?
      hours = time_data[1].to_i
      minutes = time_data[2].to_i
      time_str = hours == 0 ? "min" : "hrs"    
      if hours < 1 and minutes < 30 and get_notifier
        options = get_notifier[1] % ["Low Battery", "You have #{minutes} minutes left!"]
        if get_notifier[0] == "terminal-notifier"
          if `which rbenv`.strip.empty?
            terminal_notifier_exe = `which terminal-notifier`.strip
          else
            terminal_notifier_exe = `rbenv which terminal-notifier`.strip
          end
          unless terminal_notifier_exe.empty?
            execute = `#{terminal_notifier_exe} #{options}`
          end
        else
          execute = `#{get_notifier[0]} #{options}`
        end
      end
      ' %s[%s:%s %s]%s' % [
        $COLOR_RED,
        hours, sprintf("%02d", minutes), time_str,
        $COLOR_RESET
      ]
    end
  end
end

unless $are_we_on_ac
  puts "#{views 'hours'}"
  # puts "#{views 'icon'}"
end
