#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require "#{File.dirname(__FILE__)}/colors_prompt"
unless `which rbenv`.strip.empty?
  rbenv_version = `rbenv version-name`.strip
  unless rbenv_version == "system"
    puts "#{$COLOR_BLUE}[#{rbenv_version}]#{$COLOR_RESET} "
  end
end
