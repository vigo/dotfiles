#!/usr/bin/env ruby
# encoding: utf-8

require "#{File.dirname(__FILE__)}/colors_prompt"
ips = `ifconfig -l`.strip.split.select{ |iface|
  ! `ifconfig #{iface}`.scan(/inet (\d+.\d+\.\d+\.\d+)/).empty? && iface != "lo0"
}.map { |iface|
  {iface => `ifconfig #{iface}`.scan(/inet (\d+.\d+\.\d+\.\d+)/).flatten}
}.map{ |hash|
  hash.collect{ |iface, ip|
    ip = ip.first if ip.is_a?(Array)
    "#{$COLOR_YELLOW}#{iface}#{$COLOR_RESET}:#{$COLOR_GREEN_B}#{ip}#{$COLOR_RESET}"
  }
}.join(", ")
puts " [%s]" % ips unless ips.empty?