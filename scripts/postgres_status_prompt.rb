#!/usr/bin/env ruby
# coding: utf-8

require "#{File.dirname(__FILE__)}/colors_prompt"

unless `which pg_ctl`.strip.empty?
  postgres_status = `pg_ctl -D /usr/local/var/postgres status`.strip
  unless postgres_status.match(/server is running/).to_s.empty?
    puts "[#{$COLOR_GREEN_L}postgres#{$COLOR_RESET}] "
  end
end