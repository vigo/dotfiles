dotfiles-bash
=============

My Dotfiles for Bash...

# History
**2013-04-17**

- `battery_prompt.rb` now supports `rbenv`

**2013-03-07**

- bash function `server()` changed. Auto open url function disabled...

**2013-01-22**

- `battery_prompt.rb` now support osx internal notification. requires `gem install terminal-notifier`

**2013-01-21**

- bash completion for `bundler`

**2012-11-15**

- bash completion for `fab.py`

**2012-10-25**

- `shift+TAB` menu completion `inputrc`
- `CDPATH` variable for `env`. Here is an example `CDPATH`

    export CDPATH=$CDPATH:"~/Desktop:~/Development"

now you can make: `cd Desktop/` anywhere in the file system

**2012-09-04**

- `screen` status prompt

**2012-07-19**

- Remainin battery hours indicator has been added via `show_remaining`
  variable. `show_remaining = true` or `show_remaining = false` due to
  retina display's mbp...

**2012-07-04**

- Turkish Date settings has been removed from `shell/env`

**2012-07-03**

- "z script" has been added as submodule. you must init and update:
`git submodule init` and `git submodule update` after pull or clone

# I use these tools

* [Homebrew][homebrew]
* [rbenv][rbenv]
* [pip][pip]
* virtualenv
* virtualenvwrapper
* [npm][npm]
* [pow][pow]

All my env / bash has been configured to use them as comfortable as i can.

# Install
Clone the repo as `Dotfiles` in to `$HOME`

```
cd $HOME
git clone git://github.com/vigo/dotfiles-bash.git Dotfiles
```

Create symlinks to related files:

```shell
ln -sv ~/Dotfiles/shell/bash_profile ~/.bash_profile
ln -sv ~/Dotfiles/rc/bashrc ~/.bashrc
ln -sv ~/Dotfiles/rc/ackrc ~/.ackrc
ln -sv ~/Dotfiles/rc/gemrc ~/.gemrc
ln -sv ~/Dotfiles/rc/inputrc ~/.inputrc
ln -sv ~/Dotfiles/rc/irbrc ~/.irbrc
ln -sv ~/Dotfiles/rc/nanorc ~/.nanorc
```

Also, i use some extra gems for irb and pow. I assume that you have `rbenv`
installed:

```
gem install wirble
gem install ori
gem install awesome_print
gem install powder           # for pow
```

You can put your private libs under `private/` dir. Its in the `.gitignore`
file.

# Goodies
There are few extra candies for your prompt shell aka `PS1`. These are:

* **free_memory**: Shows current available memory in Gigabytes. (G)
* **ip_list_prompt**: Shows you current ip addresses depending on interfaces.
* **git_prompt**: Shows your git repo / branch / hashID and more...
* **rbenv_prompt**: Shows your current Ruby version.
* **battery_prompt**: If AC Power is not on, you'll see the battery information.
Original [code][battery-code] is written by [Alex McPherson][battery-alex]
* **mysql_prompt**: If `mysql.server status` is running, it prompts!
* **screen_status**: Show running `screen` sessions. [**new**]
* **postgres_status**: If `pg_ctl` exists, it checks the status and
prompts if its running [**new**]

# Example Screens

![Example Image 1](http://f.cl.ly/items/0r3q2A0h1P0r452H1c3C/dotfiles-01.png)

![Example Image 2](http://f.cl.ly/items/05383q3h1V1q1c3z2G1q/dotfiles-02.png)

![Example Image 3](http://f.cl.ly/items/0901163v223m05370C1f/dotfiles-03a.png)

![Example Image 4](http://f.cl.ly/items/3v333G1O0o1p0p092W2K/remaining_hours.png)

![Example Image for Screen 5](http://f.cl.ly/items/412C1S0k0s2M0E1G1v3Y/screen_status.png)

[homebrew]:      https://github.com/mxcl/homebrew/wiki/installation
[rbenv]:         https://github.com/sstephenson/rbenv/
[pip]:           http://www.pip-installer.org/en/latest/other-tools.html
[npm]:           http://npmjs.org/
[pow]:           http://pow.cx/
[battery-alex]:  http://quickleft.com/blog/author/alex-mcpherson
[battery-code]:  http://quickleft.com/blog/awesome-zsh-prompts-can-be-yours